FROM detochko/vscode-ssh-dev:2.1

RUN apt-get update && apt-get install \
	apt-transport-https \
	ca-certificates \
	gnupg-agent \
	software-properties-common \
	-y

RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - \
	&& add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

RUN apt-get update && apt-get install \
	docker-ce docker-ce-cli containerd.io docker-compose \
	-y

RUN apt-get autoremove -y \
	&& apt-get clean -y \
	&& rm -rf /var/lib/apt/lists/*

RUN usermod -aG docker dev && usermod -aG docker root